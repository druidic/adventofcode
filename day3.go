package main

import (
	"container/list"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type CutMarker struct {
	ID,
	X,
	Y,
	Height,
	Width int
	Overwritten bool
}

func day3Processor(line string, lineData *list.List) {
	result := CutMarker{}
	tokens := strings.Split(line, " ")
	intResult, err := strconv.Atoi(tokens[0][1:])
	if err != nil {
		logger.Errorf("Error converting id: %+v", err)
		return
	}
	result.ID = intResult
	splitTokens := strings.Split(tokens[2], ",")
	intResult, err = strconv.Atoi(splitTokens[0])
	if err != nil {
		logger.Errorf("Error converting x-coord: %+v", err)
		return
	}
	result.X = intResult
	intResult, err = strconv.Atoi(splitTokens[1][:len(splitTokens[1])-1])
	if err != nil {
		logger.Errorf("Error converting y-coord: %+v", err)
		return
	}
	result.Y = intResult
	splitTokens = strings.Split(tokens[3], "x")
	intResult, err = strconv.Atoi(splitTokens[0])
	if err != nil {
		logger.Errorf("Error converting width: %+v", err)
		return
	}
	result.Width = intResult
	intResult, err = strconv.Atoi(splitTokens[1])
	if err != nil {
		logger.Errorf("Error converting height: %+v", err)
		return
	}
	result.Height = intResult
	lineData.PushBack(&result)
}

func updateOverwritten(mapCoord string, element *CutMarker, overwritten map[string][]*CutMarker) {
	if overwritten[mapCoord] != nil {
		for i, _ := range overwritten[mapCoord] {
			overwritten[mapCoord][i].Overwritten = true
		}
		element.Overwritten = true
		overwritten[mapCoord] = append(overwritten[mapCoord], element)
	} else {
		overwritten[mapCoord] = []*CutMarker{element}
	}
}

func populateBitField(countField [][]uint16, idStruct *list.List) map[string][]*CutMarker {
	overwritten := map[string][]*CutMarker{}
	for claim := idStruct.Front(); claim != nil; claim = claim.Next() {
		element := claim.Value.(*CutMarker)
		for widthOffset := 0; widthOffset < element.Width; widthOffset += 1 {
			fieldXCoordinate := element.X + widthOffset
			for heightOffset := 0; heightOffset < element.Height; heightOffset += 1 {
				fieldYCoordinate := element.Y + heightOffset
				if countField[fieldXCoordinate] == nil {
					countField[fieldXCoordinate] = make([]uint16, 1000)
				}
				mapCoord := fmt.Sprintf("%d,%d", fieldXCoordinate, fieldYCoordinate)
				updateOverwritten(mapCoord, element, overwritten)
				countField[fieldXCoordinate][fieldYCoordinate] += 1
			}
		}
	}
	return overwritten
}

func countOverwritten(countField [][]uint16) int {
	result := 0
	for x := 0; x < 1000; x += 1 {
		for y := 0; y < 1000; y += 1 {
			if countField[x] != nil && countField[x][y] > 1 {
				result += 1
			}
		}
	}
	return result
}

// executes day 2
func execDay3(filename string, part int) {
	countField := make([][]uint16, 1000)
	fileLines, err := readFile(filename, day3Processor)
	if err != nil {
		logger.Fatalf("Error reading file: %+v", err)
		os.Exit(1)
	}
	logger.Debugf("NumId: %d fieldAddr: %p", fileLines.Len(), countField)
	populateBitField(countField, fileLines)
	if part == 1 {
		overwritten := countOverwritten(countField)
		logger.Infof("Fields overwritten: %d", overwritten)
	} else {
		var result *CutMarker
		for e := fileLines.Front(); e != nil; e = e.Next() {
			claim := e.Value.(*CutMarker)
			if !claim.Overwritten {
				result = claim
				logger.Debugf("Found ID not overwritten: %d", claim.ID)
			}
		}
		logger.Infof("Found ID: %d", result.ID)
	}
}
