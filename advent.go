package main

import (
	"bufio"
	"container/list"
	"fmt"
	"os"
	"strconv"
)

var (
	Version      string
	BuildTime    string
	MaxRoutines  string
	MAX_ROUTINES int
	days         []func(string, int)
	logger       LevelLogger
)

// reads the file of data
// returns a list comprised of int
func readFile(filename string, processor func(string, *list.List)) (*list.List, error) {
	fileItem, err := os.Open(filename)
	fileLines := list.New()
	if err != nil {
		return fileLines, err
	}
	defer fileItem.Close()
	scanner := bufio.NewScanner(fileItem)
	for scanner.Scan() {
		processor(scanner.Text(), fileLines)
	}
	return fileLines, nil
}

// inserts element e into a sorted list, calling the sort function to perform
// the sort
func insertSorted(e interface{}, elements *list.List, isLesser func(a interface{}, b *list.Element) bool) {
	if elements.Len() == 0 {
		elements.PushBack(e)
		return
	}
	for element := elements.Front(); element != nil; element = element.Next() {
		if isLesser(e, element) {
			elements.InsertBefore(e, element)
			return
		}
	}
	elements.PushBack(e)
}

// handles the program
func main() {
	var err error
	loggingLevel := os.Getenv("LOGGER_LEVEL")
	if loggingLevel == "" {
		loggingLevel = "INFO"
	}
	logger, err = initLogger(loggingLevel, "STDERR", "")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to initialize logger: %+v", err)
	}
	defer logger.Close()
	filename := os.Getenv("ADVENT_FILE")
	if filename == "" {
		logger.Fatal("Filename must be specified in ADVENT_FILE.\n")
		os.Exit(1)
	}
	MAX_ROUTINES, err = strconv.Atoi(MaxRoutines)
	if err != nil {
		logger.Fatalf("Failed to set MAX_ROUTINES: %+v", err)
		os.Exit(1)
	}
	dayNum, err := strconv.Atoi(os.Getenv("ADVENT_DAY"))
	if err != nil || dayNum == 0 {
		logger.Fatal("Day not specified. Exiting.\n")
		os.Exit(2)
	}
	partNum, err := strconv.Atoi(os.Getenv("ADVENT_PART"))
	if err != nil || partNum == 0 {
		logger.Fatal("Part not specified. Exiting.\n")
		os.Exit(2)
	}
	logger.Debugf("File: %s\tDay: %-2d\tPart: %d\n", filename, dayNum, partNum)
	days = []func(string, int){execDay1, execDay2, execDay3,
		execDay4, execDay5, execDay6,
		execDay7}
	days[dayNum-1](filename, partNum)
}
