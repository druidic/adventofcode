package main

import (
	"container/list"
	"fmt"
	"os"
	"strconv"
)

func iterateList(result int, changeList *list.List, results map[int]bool) (int, error) {
	var found error = nil
	for e := changeList.Front(); e != nil; e = e.Next() {
		result += e.Value.(int)
		_, ok := results[result]
		if ok {
			found = fmt.Errorf("Found duplicate in results: %d\n", result)
			break
		}
		results[result] = true
	}
	return result, found
}

// returns the integer frequency that
// occurs as the first duplicate
func findFreq(listData ListData) int {
	results := map[int]bool{0: true}
	result := 0
	var ok error = nil
	loopCount := 0
	searching := true
	for searching {
		result, ok = iterateList(result, listData.List, results)
		if ok != nil {
			searching = false
			fmt.Printf("%+v", ok)
		}
		loopCount += 1
	}
	fmt.Printf("Results: %5d\tLoops: %d\n", len(results), loopCount)
	return result
}

// handles parsing the data from the advent file
func parseDay1(line string, lineList *list.List) {
	num, err := strconv.Atoi(line)
	if err != nil {
		return
	}
	lineList.PushBack(num)
}

// executes day1
func execDay1(filename string, part int) {
	fileLines, err := readFile(filename, parseDay1)
	if err != nil {
		fmt.Printf("Error reading file: %+v", err)
		os.Exit(1)
	}
	listData := ListData{List: fileLines,
		IsMonotonicIncrease: true,
		IsMonotonicDecrease: true}
	fmt.Printf("Using build: %s\n", BuildTime)
	freq := findFreq(listData)
	fmt.Printf("Frequency: %d\n", freq)
	fmt.Printf("Meta: %+v\n", listData)
}
