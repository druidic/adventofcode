# Simplifies the building process

VERSION="0.0.1"
BUILD=`git rev-parse HEAD`

LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.BuildTime=${BUILD} -X main.MaxRoutines=500"

default:
	-rm stderr.log
	go fmt
	go install ${LDFLAGS}

run: default
	${GOPATH}bin/advent ${ARGS} 2>stderr.log

clean:
	go clean

