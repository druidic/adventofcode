package main

import (
	"container/list"
	"os"
	"strings"
	"sync"
)

func day5Processor(line string, listData *list.List) {
	for _, c := range line {
		listData.PushBack(int(c))
	}
}

func elementsReact(a int, b int) bool {
	diff := 97 - 65
	if b > a {
		a = a ^ b
		b = a ^ b
		a = a ^ b
	}
	logger.Debugf("Diff: %-3d a: %-3d    b: %-3d", a-b, a, b)
	return a-b == diff
}

// accepts the current index, to be compared with the next element
// after reaction, determines if previous should be removed
// second list stored in reverse order to enable easy removal
func parseList(resultList *list.List) *list.List {
	for element := resultList.Front(); element != nil; {
		nextElement := element.Next()
		if nextElement == nil {
			return resultList
		}
		a := element.Value.(int)
		b := nextElement.Value.(int)
		if elementsReact(a, b) {
			prevElement := element.Prev()
			if prevElement == nil {
				prevElement = nextElement.Next()
			}
			resultList.Remove(element)
			resultList.Remove(nextElement)
			element = prevElement
		} else {
			element = nextElement
		}
	}
	return resultList
}

func buildString(elements *list.List) string {
	var result strings.Builder
	for e := elements.Front(); e != nil; e = e.Next() {
		result.WriteString(string(e.Value.(int)))
	}
	return result.String()
}

func removeBadChars(a int, b int, element *list.Element) *list.List {
	resultList := list.New()
	for e := element; e != nil; e = e.Next() {
		val := e.Value.(int)
		if val != a && val != b {
			resultList.PushBack(val)
		}
	}
	return resultList
}

func parseForBadChars(workerGroup *sync.WaitGroup, element *list.Element, startLowerInt int, startUpperInt int, resultChan *chan int) {
	defer workerGroup.Done()
	logger.Debugf("lower: %-3d upper: %-3d", startLowerInt, startUpperInt)
	resultList := removeBadChars(startLowerInt, startUpperInt, element)
	resultList = parseList(resultList)
	logger.Infof("Len: %d", resultList.Len())
	*resultChan <- resultList.Len()
}

func execDay5(filename string, part int) {
	elements, err := readFile(filename, day5Processor)
	if err != nil {
		logger.Fatalf("Error reading file: %+v", err)
		os.Exit(1)
	}
	logger.Debugf("Elements parsed: %d", elements.Len())
	if part == 1 {
		resultList := parseList(elements)
		result := buildString(resultList)
		logger.Infof("Result: %-10s Len: %d", result, len(result))
	} else {
		startLowerInt := int('a')
		startUpperInt := int('A')
		WORKER_COUNT := 26
		resultChan := make(chan int, 26)
		var workerGroup sync.WaitGroup
		for i := 0; i < WORKER_COUNT; i += 1 {
			workerGroup.Add(1)
			go parseForBadChars(&workerGroup, elements.Front(), startLowerInt+i, startUpperInt+i, &resultChan)
		}
		workerGroup.Wait()
		shortest := <-resultChan
		for i := 0; i < WORKER_COUNT-1; i += 1 {
			e := <-resultChan
			if e < shortest {
				shortest = e
			}
		}
		logger.Infof("Shortest Length: %d", shortest)
	}
}
