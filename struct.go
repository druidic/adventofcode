package main

import "container/list"

// contains metadata about the list
type ListData struct {
	IsMonotonicIncrease,
	IsMonotonicDecrease bool
	HighVal,
	LowVal,
	Count,
	Result int
	List *list.List
}
