package main

import (
	"container/list"
	"fmt"
	"os"
	"strings"
)

// preprocesses each line for day 2
func day2Processor(line string, lineData *list.List) {
	lineData.PushBack(line)
}

// builds a frequency map of each character in the string
func buildFreqMap(line string) map[rune]int {
	freqMap := make(map[rune]int)
	for _, letter := range line {
		_, ok := freqMap[letter]
		if !ok {
			freqMap[letter] = 1
		} else {
			freqMap[letter] = freqMap[letter] + 1
		}
	}
	return freqMap
}

// returns the doubles and triples in a specified line
func getPairedChecksum(line string, doubles int, triples int) (int, int) {
	freqMap := buildFreqMap(line)
	noDouble := true
	noTriple := true
	for _, v := range freqMap {
		if noDouble && v == 2 {
			doubles += 1
			noDouble = false
		} else if noTriple && v == 3 {
			triples += 1
			noTriple = false
		}
	}
	return doubles, triples
}

// counts the characters in each line, storing the frequency count in a map
func countCharsInLines(fileLines *list.List) (doubles, triples int) {
	for e := fileLines.Front(); e != nil; e = e.Next() {
		doubles, triples = getPairedChecksum(e.Value.(string), doubles, triples)
	}
	return
}

func pairDifference(a string, b string) int {
	diffcount := 0
	for i, _ := range a {
		if a[i] != b[i] {
			diffcount += 1
		}
	}
	return diffcount
}

// returns the two pairs that differ by one piece
func checkPairs(fileLines *list.List) (string, string) {
	var pairA string
	var pairB string
	for a := fileLines.Front(); a != nil; a = a.Next() {
		pairA = a.Value.(string)
		b := a
		for b := b.Next(); b != nil; b = b.Next() {
			pairB = b.Value.(string)
			if pairDifference(pairA, pairB) == 1 {
				return pairA, pairB
			}
		}
	}
	return "", ""
}

// returns the common set of letters
func extractSameLetters(a string, b string) string {
	var builder strings.Builder
	for i, char := range a {
		if a[i] == b[i] {
			builder.WriteRune(char)
		}
	}
	return builder.String()
}

// executes day 2
func execDay2(filename string, part int) {
	fileLines, err := readFile(filename, day2Processor)
	if err != nil {
		fmt.Printf("Error reading file: %+v", err)
		os.Exit(1)
	}
	// part 1
	if part == 1 {
		doubles, triples := countCharsInLines(fileLines)
		fmt.Printf("Lines: %d\n", fileLines.Len())
		fmt.Printf("Doubles: %4d\tTriples: %4d\nResult: %d\n", doubles, triples, doubles*triples)
	} else {
		pairA, pairB := checkPairs(fileLines)
		result := extractSameLetters(pairA, pairB)
		fmt.Printf("pairA: %s pairB: %s\nLetters: %s\n", pairA, pairB, result)
	}
}
