package main

import (
	"container/list"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

type Point interface {
	XCoord() int
	YCoord() int
}

type GridBoundaries struct {
	MaxX,
	MinX,
	MaxY,
	MinY int
}

type CartesianPoint struct {
	X,
	Y int
}

type SuperPoint struct {
	CartesianPoint
	PointsOwned   *list.List
	ModOwnedMutex chan int
	uniquePoints  int
}

type OwnedPoint struct {
	CartesianPoint
	OwningPoint       []*SuperPoint
	ManhattanDistance int
}

func (p CartesianPoint) XCoord() int {
	return p.X
}

func (p CartesianPoint) YCoord() int {
	return p.Y
}

func (p CartesianPoint) String() string {
	return fmt.Sprintf("[%d, %d]", p.X, p.Y)
}

func (o OwnedPoint) UniqueOwner() bool {
	logger.Debugf("UniqueLen: %d\tx: %d y: %d", len(o.OwningPoint), o.XCoord(), o.YCoord())
	return len(o.OwningPoint) <= 1
}

func (s *SuperPoint) UniquePoints() int {
	if s.uniquePoints <= 0 {
		s.uniquePoints = 0
		for e := s.PointsOwned.Front(); e != nil; e = e.Next() {
			if e.Value.(*OwnedPoint).UniqueOwner() {
				s.uniquePoints += 1
			}
		}
	}
	logger.Debugf("uniquePts: %d\tx: %d y: %d", s.uniquePoints, s.XCoord(), s.YCoord())
	return s.uniquePoints
}

func day6Processor(line string, listData *list.List) {
	points := strings.Split(line, ", ")
	if len(points) != 2 {
		logger.Errorf("Could not parse line: %s", line)
	}
	a, err := strconv.Atoi(points[0])
	if err != nil {
		logger.Errorf("Could not parse line: %s", line)
	}
	b, err := strconv.Atoi(points[1])
	if err != nil {
		logger.Errorf("Could not parse line: %s", line)
	}
	listData.PushBack(&SuperPoint{CartesianPoint: CartesianPoint{X: a,
		Y: b},
		ModOwnedMutex: make(chan int, 1),
		PointsOwned:   list.New()})
}

func pointInBounds(point *CartesianPoint, bounds *GridBoundaries) bool {
	return point.X < bounds.MaxX && point.X > bounds.MinX &&
		point.Y < bounds.MaxY && point.Y > bounds.MinY
}

func defineGrid(coordList []*SuperPoint) *GridBoundaries {
	coords := coordList[0]
	result := GridBoundaries{MaxX: coords.X,
		MinX: coords.X,
		MaxY: coords.Y,
		MinY: coords.Y}
	for _, coords := range coordList {
		if result.MaxX < coords.X {
			result.MaxX = coords.X
		}
		if result.MinX > coords.X {
			result.MinX = coords.X
		}
		if result.MaxY < coords.Y {
			result.MaxY = coords.Y
		}
		if result.MinY > coords.Y {
			result.MinY = coords.Y
		}
	}
	return &result
}

func convertListIntoPoints(elements *list.List) []*SuperPoint {
	result := make([]*SuperPoint, elements.Len())
	i := 0
	for e := elements.Front(); e != nil; e = e.Next() {
		casted := e.Value.(*SuperPoint)
		result[i] = casted
		i += 1
	}
	return result
}

func getManhattanDistance(pointA Point, pointB Point) int {
	// get abs value by dropping top bit in 2's complement ints
	xdiff := pointA.XCoord() - pointB.XCoord()
	if xdiff < 0 {
		xdiff = -xdiff
	}
	ydiff := pointA.YCoord() - pointB.YCoord()
	if ydiff < 0 {
		ydiff = -ydiff
	}
	return xdiff + ydiff
}

func appendNewPointOwner(point *OwnedPoint, superpoint *SuperPoint) {
	point.OwningPoint = append(point.OwningPoint, superpoint)
	superpoint.PointsOwned.PushFront(point)
}

// sets the new owner of this point, removing it from the previous
// ownwer{s} if necessary
func setNewPointOwner(point *OwnedPoint, superpoint *SuperPoint) {
	for _, oldPoint := range point.OwningPoint {
		if oldPoint.PointsOwned != nil {
			for e := oldPoint.PointsOwned.Front(); e != nil; e = e.Next() {
				pointElement := e.Value.(*OwnedPoint)
				if pointElement.XCoord() == point.XCoord() &&
					pointElement.YCoord() == point.YCoord() {
					oldPoint.PointsOwned.Remove(e)
					break
				}
			}
		}
	}
	point.OwningPoint = []*SuperPoint{superpoint}
	superpoint.PointsOwned.PushFront(point)
}

//takes a point and figures out who the owner is
func definePointOwner(point *OwnedPoint, pointsList []*SuperPoint) {
	for _, superpoint := range pointsList {
		distance := getManhattanDistance(point, superpoint)
		if distance == point.ManhattanDistance {
			appendNewPointOwner(point, superpoint)
		} else if distance < point.ManhattanDistance {
			setNewPointOwner(point, superpoint)
			point.ManhattanDistance = distance
		}
	}
}

// iterate through one column of points, defining the owners for each
func defineColumnOwner(bounds *GridBoundaries, pointsList []*SuperPoint, xcoord int, workerGroup *sync.WaitGroup, notifyFinish chan bool) {
	defer workerGroup.Done()
	defer func() {
		<-notifyFinish
	}()
	maxInt := int(^uint(0)>>1) / 2
	var maxPoint SuperPoint
	maxPoint.X = maxInt
	maxPoint.Y = maxInt
	for y := bounds.MinY + 1; y < bounds.MaxY; y += 1 {
		point := OwnedPoint{CartesianPoint: CartesianPoint{X: xcoord,
			Y: y},
			OwningPoint:       []*SuperPoint{&maxPoint},
			ManhattanDistance: -1}
		point.ManhattanDistance = getManhattanDistance(point, maxPoint)
		definePointOwner(&point, pointsList)
	}
}

func definePointOwners(bounds *GridBoundaries, pointsList []*SuperPoint, notify chan int) {
	var workerGroup sync.WaitGroup
	maxRoutines := make(chan bool, MAX_ROUTINES)
	for x := bounds.MinX; x < bounds.MaxX; x += 1 {
		workerGroup.Add(1)
		maxRoutines <- true
		go defineColumnOwner(bounds, pointsList, x, &workerGroup, maxRoutines)
	}
	workerGroup.Wait()
	notify <- 1
}

func isValidPoint(bounds *GridBoundaries, point *SuperPoint) bool {
	if point.XCoord() <= bounds.MinX || point.XCoord() >= bounds.MaxX {
		return false
	}
	if point.YCoord() <= bounds.MinY || point.XCoord() >= bounds.MaxY {
		return false
	}
	logger.Debugf("%d,%d is valid", point.XCoord(), point.YCoord())
	return true
}

func execDay6(filename string, part int) {
	elements, err := readFile(filename, day6Processor)
	pointsList := convertListIntoPoints(elements)
	if err != nil {
		logger.Fatalf("Error reading file: %+v", err)
		os.Exit(1)
	}
	logger.Debugf("Elements parsed: %d", elements.Len())
	if part == 1 {
		bounds := defineGrid(pointsList)
		logger.Debugf("GridBounds: %+v, MaxPoints: %d", bounds, (bounds.MaxX-bounds.MinX)*
			(bounds.MaxY-bounds.MinY))
		notify := make(chan int, 1)
		definePointOwners(bounds, pointsList, notify)
		<-notify
		var highestCountPoint *SuperPoint
		for _, superpoint := range pointsList {
			if isValidPoint(bounds, superpoint) {
				if highestCountPoint == nil {
					highestCountPoint = superpoint
				} else if highestCountPoint.UniquePoints() < superpoint.UniquePoints() {
					highestCountPoint = superpoint
				}
			}
		}
		logger.Infof("Result: %+v with %d points", highestCountPoint, highestCountPoint.UniquePoints())
	} else {
		logger.Infof("Result: ")
	}
}
