package main

import (
	"container/list"
	"os"
	"strconv"
	"strings"
	"time"
)

const ACTION_AWAKEN = 0
const ACTION_ASLEEP = 1
const ACTION_SHIFT_START = 2

type GuardEntry struct {
	// can be one of AWAKEN(0), ASLEEP(1), SHIFT_START(2)
	Action  uint8
	Time    time.Time
	GuardID int
}

type GuardSchedule struct {
	Entries         []*GuardEntry
	Schedule        []int
	minutesAsleep   int
	highSleepMinute int
}

func (gs *GuardSchedule) HighSleepMinute() int {
	if gs.highSleepMinute <= 0 {
		gs.MinutesAsleep()
	}
	return gs.highSleepMinute
}

func (gs *GuardSchedule) MinutesAsleep() int {
	if gs.minutesAsleep <= 0 {
		var lastEntry *GuardEntry
		gs.Schedule = make([]int, 60)
		for _, entry := range gs.Entries {
			if lastEntry != nil && lastEntry.Action == ACTION_ASLEEP {
				for min := lastEntry.Time.Minute(); min < entry.Time.Minute(); min += 1 {
					gs.Schedule[min] += 1
				}
			}
			lastEntry = entry
		}
	}
	gs.minutesAsleep = 0
	gs.highSleepMinute = 0
	for minute, sleepCount := range gs.Schedule {
		gs.minutesAsleep += sleepCount
		if sleepCount > gs.Schedule[gs.highSleepMinute] {
			gs.highSleepMinute = minute
		}
	}
	return gs.minutesAsleep
}

func day4Sorter(a interface{}, b *list.Element) bool {
	aEntry := a.(GuardEntry)
	bEntry := b.Value.(GuardEntry)
	return aEntry.Time.Before(bEntry.Time)
}

func day4Processor(line string, fileLines *list.List) {
	endDateIndex := strings.Index(line, "]")
	if endDateIndex < 0 {
		logger.Errorf("Could not parse date for: %s", line)
		return
	}
	entryTime, err := time.Parse("[2006-01-02 15:04]", line[:endDateIndex+1])
	if err != nil {
		logger.Errorf("Could not parse date for: %+v {%s}", err, line)
		return
	}
	var entry GuardEntry
	entry.Time = entryTime
	if strings.Contains(line, "asleep") {
		entry.Action = ACTION_ASLEEP
	} else if strings.Contains(line, "wakes up") {
		entry.Action = ACTION_AWAKEN
	} else if strings.Contains(line, "begins shift") {
		entry.Action = ACTION_SHIFT_START
		guardStr := strings.Split(line[endDateIndex+2:], " ")[1]
		guardID, err := strconv.Atoi(guardStr[1:])
		if err != nil {
			logger.Errorf("Failed to find guard id for: %+v {%s}", err, guardStr)
			return
		}
		entry.GuardID = guardID
	} else {
		logger.Errorf("Could not locate an action for: %s", line)
		return
	}
	insertSorted(entry, fileLines, day4Sorter)
}

func sortGuards(entries *list.List) map[int]*GuardSchedule {
	guardScheduleMap := map[int]*GuardSchedule{}
	currentID := -1
	for entry := entries.Front(); entry != nil; entry = entry.Next() {
		guardEntry := entry.Value.(GuardEntry)
		if guardEntry.GuardID > 0 {
			currentID = guardEntry.GuardID
		}
		if guardScheduleMap[currentID] == nil {
			newSchedule := GuardSchedule{Entries: []*GuardEntry{&guardEntry}}
			guardScheduleMap[currentID] = &newSchedule
		} else {
			guardScheduleMap[currentID].Entries = append(guardScheduleMap[currentID].Entries, &guardEntry)
		}
	}
	return guardScheduleMap
}

func execDay4(filename string, part int) {
	elements, err := readFile(filename, day4Processor)
	if err != nil {
		logger.Fatalf("Error reading file: %+v", err)
		os.Exit(1)
	}
	logger.Debugf("Elements parsed: %d", elements.Len())
	guardSchedules := sortGuards(elements)
	if part == 1 {
		highID := -1
		for k, v := range guardSchedules {
			if highID < 0 {
				highID = k
				logger.Debugf("init highID: %d", highID)
			} else if v.MinutesAsleep() > guardSchedules[highID].MinutesAsleep() {
				logger.Debugf("highID-old: %d highID-new: %d", highID, k)
				highID = k
			}
		}
		logger.Infof("HighID : %d Min: %d", highID, guardSchedules[highID].HighSleepMinute())
	} else {
		highID := -1
		highMinuteCount := 0
		for k, v := range guardSchedules {
			if highID < 0 {
				highID = k
				highMinuteCount = v.Schedule[v.HighSleepMinute()]
				logger.Debugf("init highID: %d", highID)
			} else if v.Schedule[v.HighSleepMinute()] > highMinuteCount {
				logger.Debugf("highID-old: %d highID-new: %d", highID, k)
				highID = k
				highMinuteCount = v.Schedule[v.HighSleepMinute()]
			}
		}
		logger.Infof("HighID: %d, HighMinute: %d, ChkSum: %d", highID, guardSchedules[highID].HighSleepMinute(), highID*guardSchedules[highID].HighSleepMinute())
	}
}
