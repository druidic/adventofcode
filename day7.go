package main

import (
	"container/list"
	"os"
	"strings"
)

type OrderedStep struct {
	Precursors []string
	Value      string
}

func (os OrderedStep) String() string {
	return os.Value
}

type OrderedSteps []*OrderedStep

func (os OrderedSteps) Less(a, b int) bool {
	for _, prec := range os[a].Precursors {
		if os[b].Value == prec {
			return false
		}
	}
	return true
}

func (os OrderedSteps) Swap(a, b int) {
	os[a], os[b] = os[b], os[a]
}

func (os OrderedSteps) Len() int {
	return len(os)
}

func stepInserted(newStep *OrderedStep, step *OrderedStep, listData *list.List, pre string, e *list.Element) bool {
	compare := strings.Compare(newStep.Value, step.Value)
	if compare == 0 {
		if pre != "" {
			step.Precursors = append(step.Precursors, pre)
		}
		return true
	} else if compare < 0 {
		logger.Debugf("Insert {%s} before {%s}.", newStep.Value, step.Value)
		listData.InsertBefore(newStep, e)
		return true
	}
	return false
}

func insertSortedStep(pre string, post string, listData *list.List) {
	newStep := OrderedStep{Value: post, Precursors: []string{}}
	newStep.Precursors = append(newStep.Precursors, pre)
	tPrecursor := OrderedStep{Value: pre, Precursors: []string{}}
	postFound := false
	preFound := false
	if listData.Len() == 0 {
		listData.PushFront(&newStep)
		logger.Debugf("Pushed first list element.")
		postFound = true
	}
	for e := listData.Front(); e != nil; e = e.Next() {
		step := e.Value.(*OrderedStep)
		if !postFound {
			postFound = stepInserted(&newStep, step, listData, pre, e)
		}
		if !preFound {
			preFound = stepInserted(&tPrecursor, step, listData, "", e)
		}
		if preFound && postFound {
			return
		}
	}
	if !postFound {
		logger.Debugf("Created new step: %+v", newStep)
		listData.PushBack(&newStep)
	}
	if !preFound {
		logger.Debugf("Pushed pre: %s", tPrecursor.Value)
		listData.PushBack(&tPrecursor)
	}
}

func convertListToOrderedSteps(listData *list.List) []*OrderedStep {
	result := make([]*OrderedStep, listData.Len())
	index := 0
	for e := listData.Front(); e != nil; e = e.Next() {
		result[index] = e.Value.(*OrderedStep)
		index += 1
	}
	return result
}

func day7Processor(line string, listData *list.List) {
	precursorIndex := 1
	postIndex := 7
	tokens := strings.Split(line, " ")
	if len(tokens) < postIndex || len(tokens) < precursorIndex {
		logger.Errorf("Failed to parse line: %s", line)
		return
	}
	prec := tokens[precursorIndex]
	post := tokens[postIndex]
	insertSortedStep(prec, post, listData)
}

func swapToLocation(initial, end int, orderedSteps OrderedSteps) {
	for i := initial; i > end && i >= 0; i -= 1 {
		logger.Debugf("i: %d init: %d end: %d el: %s tar: %s", i, initial,
			end, orderedSteps[initial], orderedSteps[end])
		orderedSteps.Swap(i, i-1)
	}
}

func (s OrderedSteps) InPrecursorList(item, checkList int) bool {
	for _, pre := range s[item].Precursors {
		if pre == s[checkList].Value {
			return true
		}
	}
	return false
}

func findTargetIndex(currentElement int, steps OrderedSteps) int {
	for i := 0; i < currentElement && i < len(steps); i += 1 {
		if steps.InPrecursorList(currentElement, i) {
			return i
		}
	}
	return currentElement
}

func sortSteps(orderedSteps []*OrderedStep) {
	steps := OrderedSteps(orderedSteps)
	currentElement := len(orderedSteps) - 1
	for currentElement > 0 {
		target := findTargetIndex(currentElement, steps)
		logger.Debugf("cI: %d tI: %d c: %s t: %s", currentElement, target,
			steps[currentElement], steps[target])
		if target != currentElement {
			swapToLocation(currentElement, target, steps)
		}
		currentElement -= 1
	}
}

func execDay7(filename string, part int) {
	elements, err := readFile(filename, day7Processor)
	if err != nil {
		logger.Fatalf("Error reading file: %+v", err)
		os.Exit(1)
	}
	orderedSteps := convertListToOrderedSteps(elements)
	logger.Debugf("Elements parsed: %d %+v", len(orderedSteps), orderedSteps)
	if part == 1 {
		sortSteps(orderedSteps)
		logger.Infof("Result: %+v", orderedSteps)
	} else {
		logger.Infof("Result: ")
	}
}
